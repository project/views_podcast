<?php

/**
 * Provide views plugins for the feed types we support.
 */
function views_podcast_views_style_plugins() {
  return array(
    'views_podcast' => array(
      'name' => t('Views Podcast: Podcast feed'),
      'theme' => 'views_podcast_feed',
      'needs_table_header' => TRUE,
      'needs_fields' => TRUE,
      'even_empty' => TRUE,
    ),
  );
}

/**
 * While we support the global selector, some might want to allow
 * ONLY Podcast feeds so we support a stingy selector too
 */
function views_podcast_views_arguments() {
  $arguments = array(
    'podcast_feed' => array(
      'name' => t('Views Podcast: Podcast feed'),
      'handler' => 'views_handler_arg_podcast',
      'help' => t('This argument specifies a specific Podcast feed selector; it will only select Podcast feeds, unlike the built-in selector which can select pluggable feeds.'),
    ),
  );
  return $arguments;
}

/**
 * handler for our own Podcast argument; mimics the feed selector
 */
function views_handler_arg_podcast_feed($op, &$query, $argtype, $arg = '') {
  switch($op) {
    case 'summary':
    case 'sort':
    case 'link':
    case 'title':
      break;
    case 'filter':
      // This is a clone of the default selector, but it just invokes ours
      // rather than calling all of them.
      views_rss_views_feed_argument('argument', $GLOBALS['current_view'], $arg);
  }
}

/**
 * post view for our own op -- mimics the feed selector
 */
function views_podcast_views_post_view($view, $items, $output) {
  foreach ($view->argument as $id => $argument) {
    if ($argument['type'] == 'podcast_feed') {
      $feed = $id;
      break;
    }
  }

  if ($feed !== NULL) {
    return views_rss_views_feed_argument('post_view', $view, 'podcast_feed');
  }
}

/**
 * feed argument hook that will convert us to Podcast or display an icon.
 * the 4th argument isn't part of the hook, but we use it to differentiate
 * when called as a hook or when called manually from views_rss_views_post_view
 */
function views_podcast_views_feed_argument($op, &$view, $arg) {
  if ($op == 'argument' && $arg == 'feed') {
    $view->page_type = 'views_podcast';
  }
  else if ($op == 'post_view') {
    $args = views_post_view_make_args($view, $arg, 'feed');
    $url = views_get_url($view, $args);
    $title = views_get_title($view, 'page', $args);

    if ($view->used_filters) {
      $filters = drupal_query_string_encode($view->used_filters);
    }

    drupal_add_feed(url($url, $filters), $title);
  }
}

/**
 * plugin that actually displays an Podcast feed
 */
function theme_views_podcast_feed($view, $nodes, $type) {
  if ($type == 'block') {
    return;
  }
  global $base_url;

  $view_podcast = db_fetch_object(db_query("SELECT * FROM {views_podcast_podcasts} WHERE view_vid = %d", $view->vid));
 	
  $channel = array(
    // a check_plain isn't required on these because format_rss_channel
    // already does this.
    'title'       => $view_podcast->channel_title,
    'link'        => url($view->real_url, NULL, NULL, true),
    'description' => $view_podcast->channel_itunes_subtitle,
  );
  $channel_additional = array();
    if (isset($view_podcast->channel_copyright) && 
        $view_podcast->channel_copyright != "") {
      $channel_additional = array_merge($channel_additional, 
                            array('copyright'              => $view_podcast->channel_copyright));
    }
    if (isset($view_podcast->channel_itunes_author) && 
        $view_podcast->channel_itunes_author != "") {
      $channel_additional = array_merge($channel_additional, 
                            array('itunes:author'          => $view_podcast->channel_itunes_author));
    }
    if (isset($view_podcast->channel_itunes_block) && 
        $view_podcast->channel_itunes_block != "-1") {
      $channel_additional = array_merge($channel_additional, 
                            array('itunes:block'           => $view_podcast->channel_itunes_block?"Yes":"No"));
    }
    if (isset($view_podcast->channel_itunes_explicit) && 
        $view_podcast->channel_itunes_explicit != "-1") {
      $channel_additional = array_merge($channel_additional, 
                            array('itunes:explicit'        => $view_podcast->channel_itunes_explicit?"Yes":"No"));
    }
    if (isset($view_podcast->channel_itunes_keywords) && 
        $view_podcast->channel_itunes_keywords != "") {
      $channel_additional = array_merge($channel_additional, 
                            array('itunes:keywords'        => $view_podcast->channel_itunes_keywords));
    }
    if ((isset($view_podcast->channel_itunes_owner_name) && 
        $view_podcast->channel_itunes_owner_name != "") ||
        (isset($view_podcast->channel_itunes_owner_email) && 
        $view_podcast->channel_itunes_owner_email != "")) {
        
      $temp = array();
      if (isset($view_podcast->channel_itunes_owner_name) && 
          $view_podcast->channel_itunes_owner_name != "") {
        $temp = array_merge($temp, 
                              array('itunes:name'      => $view_podcast->channel_itunes_owner_name));
      }
      if (isset($view_podcast->channel_itunes_owner_email) && 
          $view_podcast->channel_itunes_owner_email != "") {
        $temp = array_merge($temp, 
                              array('itunes:email'     => $view_podcast->channel_itunes_owner_email));
      }
      $channel_additional = array_merge($channel_additional, 
                            array('itunes:owner'   => $temp));
    }
    if (isset($view_podcast->channel_itunes_manager_email) && 
        $view_podcast->channel_itunes_manager_email != "") {
      $channel_additional = array_merge($channel_additional, 
                            array('managingEditor'   => $view_podcast->channel_itunes_manager_email));
    }
    if (isset($view_podcast->channel_itunes_webmaster_email) && 
        $view_podcast->channel_itunes_webmaster_email != "") {
      $channel_additional = array_merge($channel_additional, 
                            array('webMaster' => $view_podcast->channel_itunes_webmaster_email));
    }
    if (isset($view_podcast->channel_itunes_subtitle) && 
        $view_podcast->channel_itunes_subtitle != "") {
      $channel_additional = array_merge($channel_additional, 
                            array('itunes:subtitle'        => $view_podcast->channel_itunes_subtitle));
    }
    if (isset($view_podcast->channel_itunes_summary) && 
        $view_podcast->channel_itunes_summary != "") {
      $channel_additional = array_merge($channel_additional, 
                            array('itunes:summary'         => html_entity_decode($view_podcast->channel_itunes_summary, ENT_QUOTES)));
    }

//Channel elements that I didn't know format_xml_elements could handle when I wrote them.
    if (isset($view_podcast->channel_itunes_category) && 
        $view_podcast->channel_itunes_category != "") {
      $temp = explode(" -> ", $view_podcast->channel_itunes_category);
      if (count($temp) == 1) {
        $items .= ' <itunes:category text="'.check_plain($temp[0]).'"/>'."\n";
      }
      else {
        $items .= ' <itunes:category text="'.check_plain($temp[0]).'">'."\n";
        $items .= '  <itunes:category text="'.check_plain($temp[1]).'"/>'."\n";
        $items .= ' </itunes:category>'."\n";
      }
    }
    if (isset($view_podcast->channel_itunes_category2) && 
        $view_podcast->channel_itunes_category2 != "") {
      $temp = explode(" -> ", $view_podcast->channel_itunes_category2);
      if (count($temp) == 1) {
        $items .= ' <itunes:category text="'.check_plain($temp[0]).'"/>'."\n";
      }
      else {
        $items .= ' <itunes:category text="'.check_plain($temp[0]).'">'."\n";
        $items .= '  <itunes:category text="'.check_plain($temp[1]).'"/>'."\n";
        $items .= ' </itunes:category>'."\n";
      }
    }
    if (isset($view_podcast->channel_itunes_category3) && 
        $view_podcast->channel_itunes_category3 != "") {
      $temp = explode(" -> ", $view_podcast->channel_itunes_category3);
      if (count($temp) == 1) {
        $items .= ' <itunes:category text="'.check_plain($temp[0]).'"/>'."\n";
      }
      else {
        $items .= ' <itunes:category text="'.check_plain($temp[0]).'">'."\n";
        $items .= '  <itunes:category text="'.check_plain($temp[1]).'"/>'."\n";
        $items .= ' </itunes:category>'."\n";
      }
    }
    if (isset($view_podcast->channel_itunes_image) && 
        $view_podcast->channel_itunes_image != "") {
      $items .= ' <itunes:image href="'.$view_podcast->channel_itunes_image.'" />'."\n";
    }

  $item_length = variable_get('feed_item_length', 'teaser');
  $namespaces = array('xmlns:dc="http://purl.org/dc/elements/1.1/"');

  // Except for the original being a while and this being a foreach, this is
  // completely cut & pasted from node.module.
  foreach ($nodes as $node) {
    // Load the specified node:
    $item = node_load($node->nid);
    $link = url("node/$node->nid", NULL, NULL, 1);
    //$link = '';  //Podcast Module spec did not request a link (But Spec did)


    if ($item_length != 'title') {
      $teaser = ($item_length == 'teaser') ? TRUE : FALSE;

      // Filter and prepare node teaser
      if (node_hook($item, 'view')) {
        node_invoke($item, 'view', $teaser, FALSE);
      }
      else {
        $item = node_prepare($item, $teaser);
      }

      // Allow modules to change $node->teaser before viewing.
      node_invoke_nodeapi($item, 'view', $teaser, FALSE);
    }
    
    // Allow modules to add additional item fields
    $extra = node_invoke_nodeapi($item, 'podcast item');

    //Prepare Item Title
    if (isset($view_podcast->item_title_override) && 
        $view_podcast->item_title_override != "") {
      $title = $view_podcast->item_title_override;
    }
    else if (isset($view_podcast->item_title) && 
            $view_podcast->item_title != "") {
      $tempa = str_replace(".", "_", $view_podcast->item_title);
      $tempb = eregi_replace(".*\.", "", $view_podcast->item_title);
      if (isset($node->$tempa)) {
        $title = $node->$tempa;  //Field exists in View data
      } 
      else if (isset($item->$tempb)) {
        $title = $item->$tempb;  //Field exists in Node data
      } 
      else {
        $title = '';
      }
    }
    else {
      $title = '';
    }
    
    // Prepare the item description
    if (isset($view_podcast->item_itunes_summary_override) && 
        $view_podcast->item_itunes_summary_override != "") {
      $description = $view_podcast->item_itunes_summary_override;
    }
    else if (isset($view_podcast->item_itunes_summary) && 
            $view_podcast->item_itunes_summary != "") {
      $tempa = str_replace(".", "_", $view_podcast->item_itunes_summary);
      $tempb = eregi_replace(".*\.", "", $view_podcast->item_itunes_summary);
      if (isset($node->$tempa)) {
        $description = $node->$tempa;  //Field exists in View data
      } 
      else if (isset($item->$tempb)) {
        $description = $item->$tempb;  //Field exists in Node data
      } 
      else {
        $description = '';
      }
    }
    else {
      $description = '';
    }
    
    $privateExtra = array();

    // Prepare the item duration (NOTE: This must be the first $privateExtra field
    if (isset($view_podcast->item_itunes_duration_override) && 
        $view_podcast->item_itunes_duration_override != "") {
      $privateExtra[] = array(
        'key' => 'itunes:duration', 
        'value' =>  $view_podcast->item_itunes_duration_override);
    }
    else if (isset($view_podcast->item_itunes_duration) && 
            $view_podcast->item_itunes_duration != "") {
      $tempa = str_replace(".", "_", $view_podcast->item_itunes_duration);
      $tempb = eregi_replace(".*\.", "", $view_podcast->item_itunes_duration);
      if (isset($node->$tempa) && eregi('(([0-9]{1,2}:[0-9]{1,2}|[0-9]{1,2}:[0-9]{1,2}:[0-9]{1,2})|[0-9]{1,6})', $node->$tempa)) {
        $privateExtra[] = array(
          'key' => 'itunes:duration', 
          'value' =>  $node->$tempa);  //Field exists in View data
      } 
      else if (isset($item->$tempb) && eregi('(([0-9]{1,2}:[0-9]{1,2}|[0-9]{1,2}:[0-9]{1,2}:[0-9]{1,2})|[0-9]{1,6})', $item->$tempb)) {
        $privateExtra[] = array(
          'key' => 'itunes:duration', 
          'value' =>  $item->$tempb);  //Field exists in Node data
      } 
      else {
        $privateExtra[] = array(
          'key' => 'itunes:duration', 
          'value' =>  '0');
      }
    }
    else {
      $privateExtra[] = array(
          'key' => 'itunes:duration', 
          'value' =>  '0');
    }

    // Prepare the item pubDate
    if (isset($view_podcast->item_pubDate_override) && 
        $view_podcast->item_pubDate_override != "") {
      $privateExtra[] = array(
        'key' => 'pubDate', 
        'value' =>  $view_podcast->item_pubDate_override);  
    }
    else if (isset($view_podcast->item_pubDate) && 
            $view_podcast->item_pubDate != "") {
      $tempa = str_replace(".", "_", $view_podcast->item_pubDate);
      $tempb = eregi_replace(".*\.", "", $view_podcast->item_pubDate);
      if (isset($node->$tempa) && 
         ($node->$tempa) >= 0 && ($node->$tempa) <= 2145916799) {
        $time = is_numeric($node->$tempa) ? $node->$tempa : strtotime($node->$tempa);
        $privateExtra[] = array(
          'key' => 'pubDate', 
          'value' =>  date('r', $time));  //Field exists in View data
      } 
      else if (isset($item->$tempb) && 
         ($item->$tempb) >= 0 && ($item->$tempb) <= 2145916799) {
        $time = is_numeric($node->$tempb) ? $node->$tempb : strtotime($node->$tempb);
        $privateExtra[] = array(
          'key' => 'pubDate', 
          'value' =>  date('r', $time));  //Field exists in Node data
      }
    }

    // Prepare the item author
    if (isset($view_podcast->item_itunes_author_override) && 
        $view_podcast->item_itunes_author_override != "") {
      $privateExtra[] = array(
        'key' => 'itunes:author', 
        'value' =>  $view_podcast->item_itunes_author_override);  
    }
    else if (isset($view_podcast->item_itunes_author) && 
            $view_podcast->item_itunes_author != "") {
      $tempa = str_replace(".", "_", $view_podcast->item_itunes_author);
      $tempb = eregi_replace(".*\.", "", $view_podcast->item_itunes_author);
      if (isset($node->$tempa)) {
        $privateExtra[] = array(
          'key' => 'itunes:author', 
          'value' =>  $node->$tempa);  //Field exists in View data
      } 
      else if (isset($item->$tempb)) {
        $privateExtra[] = array(
          'key' => 'itunes:author', 
          'value' =>  $item->$tempb);  //Field exists in Node data
      }
    }

    // Prepare the item block
    if (isset($view_podcast->item_itunes_block) && 
            $view_podcast->item_itunes_block != "") {
      $tempa = str_replace(".", "_", $view_podcast->item_itunes_block);
      $tempb = eregi_replace(".*\.", "", $view_podcast->item_itunes_block);
      if (isset($node->$tempa) && eregi("(yes|no)", $node->$tempa)) {
        $privateExtra[] = array(
          'key' => 'itunes:block', 
          'value' =>  $node->$tempa);  //Field exists in View data
      } 
      else if (isset($item->$tempb) && eregi("(yes|no)", $item->$tempb)) {
        $privateExtra[] = array(
          'key' => 'itunes:block', 
          'value' =>  $item->$tempb);  //Field exists in Node data
      }
    }
        
    // Prepare the item explicit
    if (isset($view_podcast->item_itunes_explicit) && 
            $view_podcast->item_itunes_explicit != "") {
      $tempa = str_replace(".", "_", $view_podcast->item_itunes_explicit);
      $tempb = eregi_replace(".*\.", "", $view_podcast->item_itunes_explicit);
      if (isset($node->$tempa) && eregi("(yes|no)", $node->$tempa)) {
        $privateExtra[] = array(
          'key' => 'itunes:explicit', 
          'value' =>  $node->$tempa);  //Field exists in View data
      } 
      else if (isset($item->$tempb) && eregi("(yes|no)", $item->$tempb)) {
        $privateExtra[] = array(
          'key' => 'itunes:explicit', 
          'value' =>  $item->$tempb);  //Field exists in Node data
      }
    }

    // Prepare the item keywords
    if (isset($view_podcast->item_itunes_keywords_override) && 
        $view_podcast->item_itunes_keywords_override != "") {
      $privateExtra[] = array(
        'key' => 'itunes:keywords', 
        'value' =>  $view_podcast->item_itunes_keywords_override);  
    }
    else if (isset($view_podcast->item_itunes_keywords) && 
            $view_podcast->item_itunes_keywords != "") {
      $tempa = str_replace(".", "_", $view_podcast->item_itunes_keywords);
      $tempb = eregi_replace(".*\.", "", $view_podcast->item_itunes_keywords);
      if (isset($node->$tempa)) {
        $privateExtra[] = array(
          'key' => 'itunes:keywords', 
          'value' =>  $node->$tempa);  //Field exists in View data
      } 
      else if (isset($item->$tempb)) {
				foreach ($item->taxonomy as $key => $value) {
					$array[] = $value->$tempb;
				}
        $privateExtra[] = array(
          'key' => 'itunes:keywords', 
          'value' =>  implode(", ",$array));  //Field exists in Node data
      }
    }
		
    // Prepare the item subtitle
    if (isset($view_podcast->item_itunes_subtitle_override) && 
        $view_podcast->item_itunes_subtitle_override != "") {
      $privateExtra[] = array(
        'key' => 'itunes:subtitle', 
        'value' =>  html_entity_decode($view_podcast->item_itunes_subtitle_override, ENT_QUOTES));  
    }
    else if (isset($view_podcast->item_itunes_subtitle) && 
            $view_podcast->item_itunes_subtitle != "") {
      $tempa = str_replace(".", "_", $view_podcast->item_itunes_subtitle);
      $tempb = eregi_replace(".*\.", "", $view_podcast->item_itunes_subtitle);
      if (isset($node->$tempa)) {
        $privateExtra[] = array(
          'key' => 'itunes:subtitle', 
          'value' =>  html_entity_decode($node->$tempa, ENT_QUOTES));  //Field exists in View data
      } 
      else if (isset($item->$tempb)) {
        $privateExtra[] = array(
          'key' => 'itunes:subtitle', 
          'value' =>  html_entity_decode($item->$tempb, ENT_QUOTES));  //Field exists in Node data
      }
    }
    
    // Prepare the item subtitle
    if (isset($view_podcast->item_itunes_summary_override) && 
        $view_podcast->item_itunes_summary_override != "") {
      $privateExtra[] = array(
        'key' => 'itunes:summary', 
        'value' =>  html_entity_decode($view_podcast->item_itunes_summary_override, ENT_QUOTES));  
    }
    else if (isset($view_podcast->item_itunes_summary) && 
            $view_podcast->item_itunes_summary != "") {
      $tempa = str_replace(".", "_", $view_podcast->item_itunes_summary);
      $tempb = eregi_replace(".*\.", "", $view_podcast->item_itunes_summary);
      if (isset($node->$tempa)) {
        $privateExtra[] = array(
          'key' => 'itunes:summary', 
          'value' =>  html_entity_decode($node->$tempa, ENT_QUOTES));  //Field exists in View data
      } 
      else if (isset($item->$tempb)) {
        $privateExtra[] = array(
          'key' => 'itunes:summary', 
          'value' =>  html_entity_decode($item->$tempb, ENT_QUOTES));  //Field exists in Node data
      }
    }

    // Prepare the item guid
    if (isset($view_podcast->item_guid) && 
            $view_podcast->item_guid != "") {
      $tempa = str_replace(".", "_", $view_podcast->item_guid);
      $tempb = eregi_replace(".*\.", "", $view_podcast->item_guid);
      if (isset($node->$tempa)) {
        $privateExtra[] = array(
          'key' => 'guid', 
          'value' =>  $node->$tempa.' at '.$base_url, 
          'attributes' => array('isPermaLink' => 'false'));  //Field exists in View data
      } 
      else if (isset($item->$tempb)) {
        $privateExtra[] = array(
          'key' => 'guid', 
          'value' =>  $item->$tempb.' at '.$base_url, 
          'attributes' => array('isPermaLink' => 'false'));  //Field exists in Node data
      } 
      else {
        $privateExtra = array(
            'key' => 'guid', 
            'value' => $item->nid . ' at ' . $base_url, 
            'attributes' => array('isPermaLink' => 'false'));
      }
    }
    else {
      $privateExtra = array(
          'key' => 'guid', 
          'value' => $item->nid . ' at ' . $base_url, 
          'attributes' => array('isPermaLink' => 'false'));
    }

    // Prepare the item enclosure tag
    $temp = '';
    $tempa = str_replace(".", "_", $view_podcast->url_episode);
    $tempb = eregi_replace(".*\.", "", $view_podcast->url_episode);
    if (isset($node->$tempa)) {
      $temp = $node->$tempa;  //Field exists in View data
    } 
    else if (isset($item->$tempb)) {
      $temp = $item->$tempb;  //Field exists in Node data
    }
    if ($temp != '') {
      $temp = str_replace("{episode}", drupal_urlencode($temp), strtolower($view_podcast->url_string)).$view_podcast->url_extenstion;
      
      switch ($view_podcast->url_extenstion) {
        case '.mp3':
          $tempe = 'audio/mpeg';
          break;
        case '.m4a':
          $tempe = 'audio/x-m4a';
          break;
        case '.mp4':
          $tempe = 'video/mp4';
          break;
        case '.m4v':
          $tempe = 'video/x-m4v';
          break;
        case '.mov':
          $tempe = 'video/quicktime';
          break;
        case '.flv':
          $tempe = 'video/x-flv';
          break;
        case '.pdf':
          $tempe = 'application/pdf';
          break;
        default:
          $tempe = false;
      }
    
      if ($tempe !== false && valid_url($temp)) {
        if (eregi('([0-9]{1,2}:[0-9]{1,2}|[0-9]{1,2}:[0-9]{1,2}:[0-9]{1,2})', $privateExtra[0]['value'])) {
          $tempa = explode(":", $privateExtra[0]['value']);
          $tempb = 0;
          for ($i = 0; $i < count($tempa); $i++) {
            $tempb = ($tempb*60)+$tempa[$i];
          }
          $tempd = $tempb;
        }
        else if (eregi('([0-9]{1,6})', $privateExtra[0]['value'])) {
          $tempd = $privateExtra[0]['value'];
        }
        else {
          $tempd = 0;
        }
      
        $privateExtra[] = array(
            'key' => 'enclosure',
            'attributes' => array(
                      'url' => $temp,
                      'length' => $tempd,
                      'type' => $tempe
            ));
        $temp = true;
      }
      else {
        $temp = false;
      }          
    }
    else {
      $temp = false;
    }

    $extra = array_merge($extra, $privateExtra);
    foreach ($extra as $element) {
      if ($element['namespace'] && is_array($element['namespace'])) {
        $namespaces = array_merge($namespaces, $element['namespace']);
      } else if ($element['namespace']) {
        $namespaces = array_merge($namespaces, array($element['namespace']));
      }
    }
    if ($temp) {
      $items .= format_rss_item($title, $link, $description, $extra);
    }
  }

  $channel_defaults = array(
    'version'     => '2.0',
    'title'       => variable_get('site_name', 'drupal') .' - '. variable_get('site_slogan', ''),
    'link'        => $base_url,
    'description' => variable_get('site_mission', ''),
    'language'    => $GLOBALS['locale'],
  );
  $channel = array_merge($channel_defaults, $channel);

  $output = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
  $output .= "<rss xmlns:itunes=\"http://www.itunes.com/dtds/podcast-1.0.dtd\" version=\"2.0\">\n";
  $output .= format_rss_channel($channel['title'], $channel['link'], $channel['description'], $items, $channel['language'], $channel_additional);
  $output .= "</rss>\n";
	
  drupal_set_header('Content-Type: text/xml; charset=utf-8');
  print $output;
  module_invoke_all('exit');
  exit; 
}
